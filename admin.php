<?php
function load($con,$xpt)
{
	$result=$con->selectAll();
	if($result->num_rows>0)
	{
		while($row = $result->fetch_assoc()) {
            $xpt->assign("DATA",$row);
            $xpt->assign("LINK_EDIT","edit.php?idedit=".$row["id"]);
            $xpt->assign("LINK_ADD","add.php");
            $xpt->assign("LINK_DEL","admin.php?iddel=".$row["id"]);
            $xpt->parse("main.news.row");
        }
	}
    $xpt->parse("main.news");
}
function createThumb($src,$dest,$width,$height,$type)
{

    if($type=="image/png") {
        $source_image = imagecreatefrompng($src);
        $real_width = imagesx($source_image);
        $real_height = imagesy($source_image);
        $virtual_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $width, $height, $real_width, $real_height);
        imagepng($virtual_image, $dest);
    }
    else {
        $source_image = imagecreatefromjpeg($src);
        $real_width = imagesx($source_image);
        $real_height = imagesy($source_image);
        $virtual_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $width, $height, $real_width, $real_height);
        imagejpeg($virtual_image, $dest);
    }
}
function handleImage()
{
    $infor_image=array($_FILES["image"]["type"]);
    if ($_FILES["image"]["name"] != NULL) {
        if ($_FILES["image"]["type"] == "image/jpeg" || $_FILES["image"]["type"] == "image/png" || $_FILES["image"]["type"] == "image/gif") {
            $path = "uploads/";
            $tmp_name = $_FILES["image"]["tmp_name"];
            $name = $_FILES["image"]["name"];
            $image_url = $path . $name;
            move_uploaded_file($tmp_name, $path . $name);
            array_push($infor_image, $image_url);
            return $infor_image;
        } else {
            array_push($infor_image, "uploads/default.jpg");
            return $infor_image;
        }
    }
    array_push($infor_image, "uploads/default.jpg");
    return $infor_image;
}
function HandleNews($handlenews,$xpt){
    switch (true) {
        case (filter_input(INPUT_POST, "title") == FALSE && filter_input(INPUT_GET, "iddel") == FALSE && filter_input(INPUT_GET, "idedit") == FALSE):
            load($handlenews,$xpt);
            break;
        case (filter_input(INPUT_POST, "title") != FALSE && filter_input(INPUT_GET, "idedit") == FALSE):
            $infor_image=handleImage();
            $result = $handlenews->insertRows(filter_input(INPUT_POST, "title"), filter_input(INPUT_POST, "content"), $infor_image[1]);
            if ($result != 0) {
                load($handlenews,$xpt);
                createThumb($infor_image[1],"thumbnail/".$result,200,200,$infor_image[0]);
                $xpt->assign("MES","inserted");
                $xpt->parse("main.messege");
            } else{
                $xpt->assign("MES","insert failed");
                $xpt->parse("main.messege");
            }
            break;
        case (filter_input(INPUT_GET, "iddel") != FALSE):
            $result = $handlenews->deleteById(filter_input(INPUT_GET, "iddel"));
            if ($result == true) {
                load($handlenews,$xpt);
                $xpt->assign("MES","deleted");
                $xpt->parse("main.messege");
            } else
            {
                $xpt->assign("MES","delete failed");
                $xpt->parse("main.messege");
            }
            break;
        case (filter_input(INPUT_GET, "idedit") != FALSE):
            $infor_image=handleImage();
            $result = $handlenews->editById(filter_input(INPUT_GET, "idedit"), filter_input(INPUT_POST, "title"), filter_input(INPUT_POST, "content"), $infor_image[1]);
            if ($result == true) {
                load($handlenews,$xpt);
                createThumb($infor_image[1],"thumbnail/".filter_input(INPUT_GET, "idedit"),200,200,$infor_image[0]);
                $xpt->assign("MES","edited");
                $xpt->parse("main.messege");
            } else{
                $xpt->assign("MES","edit failed");
                $xpt->parse("main.messege");
            }
            break;
    }
}
//var_dump($_GET);
//var_dump($_POST);
require_once "xtemplate.class.php";
require "HandleNews.php";
$xpt=new XTemplate("admin.html","template/");
$handlenews=new HandleNews("localhost","root","mysql","BT1");
HandleNews($handlenews,$xpt);
$xpt->parse("main");
$xpt->out("main");
$handlenews->close();
?>
