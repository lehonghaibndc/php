<?php
require_once "xtemplate.class.php";
require "HandleNews.php";
$xpt=new XTemplate("content.html","template/");
$handlenews = new HandleNews("localhost", "root", "mysql", "BT1");
$id = filter_input(INPUT_GET, "id");
if($id!=FALSE)
{
    $result = $handlenews->selectById($id);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $xpt->assign("DATA",$row);
        $xpt->assign("LINK","thumbnail/".$row["id"]);
        $xpt->parse("main.content");
    }
    else{
        $xpt->assign("MES","This Page not exist");
        $xpt->parse("main.content");
    }
}
$xpt->parse("main");
$xpt->out("main");
$handlenews->close();
?>
