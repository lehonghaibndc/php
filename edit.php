<?php
require_once "xtemplate.class.php";
require "HandleNews.php";
$xpt=new XTemplate("edit.html","template/");
$idedit = filter_input(INPUT_GET, "idedit");
$handlenews = new HandleNews("localhost", "root", "mysql", "BT1");
$result = $handlenews->selectById($idedit);
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $xpt->assign("LINK","admin.php?idedit=".$idedit);
    $xpt->assign("DATA",$row);
    $xpt->parse("main.content");
}
$xpt->parse("main");
$xpt->out("main");
$handlenews->close();
?>