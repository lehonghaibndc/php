<?php
class DBquery extends mysqli {
    public $host,$user,$password,$database,$connection;
    public function __construct($host,$user,$password,$database)
    {
        $this -> host = $host;
        $this -> user = $user;
        $this -> password = $password;
        $this -> database = $database;
        $this -> connect_me();
    }
    private function connect_me()
    {
        $this -> connection = $this-> connect($this->host,$this->user,$this->password,$this->database);
        if( $this -> connect_error )
            die($this->connect_error);
    }
    public function insert($table,$data)
    {
        $this -> query("INSERT INTO".$table."VALUES".$data);
        if($this -> error)
            return 0;
        else
            return $this->insert_id;
    }
    public function update($table,$data,$where)
    {
        $this -> query("UPDATE ".$table." SET ".$data." WHERE ".$where);
        if($this -> error)
            return false;
        else
            return true;
    }
    public function select($table,$where)
    {
        $result = $this -> query("SELECT * FROM ".$table." WHERE ".$where);
        if($this -> error)
        {
            return 0;
        }
        else
        {
            return $result;
        }
    }
    public function delete($table,$where)
    {
        $this -> query("DELETE FROM ".$table." WHERE ".$where."");
        if($this -> error)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
?>